
# Hands-On Thermoplastic

## Not too HOT to handle!

Try your hand at shaping PCL!

  * More flexible than polymer clay.
  * More useful than Silly Putty.
  * Easier than a 3D printer.

---- 

This repository holds some copy to accompany a maker-event demonstration of the properties of the thermoplastic polycaprolactone (PCL)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/2.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/2.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/2.0/">Creative Commons Attribution-ShareAlike 2.0 Generic License</a>.
