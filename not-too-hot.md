
# Hands-On Thermoplastic

## Not too HOT to handle!

Try your hand at shaping PCL!

  * More flexible than polymer clay.
  * More useful than Silly Putty.
  * Easier than a 3D printer.

----

### How does it work?

  * With just a little warmth, this material becomes putty in your hands, to shape as you wish.
  * Let it cool & it becomes tough & rigid, holding  its shape.
  * Warm it back up to try again!

----

### What is it?

#### A **polymer**

  * Its generic name is *poly(caprolactone)*, but we just call it **PCL** for short.
  * It is made of long chains of small molecules all hooked together.
    * We call this type of material a **polymer**.
  * Manufactured polymers are often just called **plastics.**

----

#### Thermoplastics

  * Plastics that can be shaped when warm but which become less flexible when cool are called **thermoplastics**.
  * PCL is sold under a number of different trade names.
  * PCL is a close relative to thermoplastics used in a very common type of 3D printer (*fused-deposition modelling*).

----

### Where do we find other polymers?

#### 3D Printing


  * There is one big difference between PCL and most 3D printing plastics: The  working temperature for those plastics (like ABS and PLA) *are* too hot to handle--you could easily get burned if you tried to shape them by hand!
  * PCL is a great way to get a real feel for how these printers work.

In essence **you** become the 3D printer with PCL!

  * **ABS** = acrylonitrile-butadiene-styrene copolymer
  * **PLA** = poly(lactic acid)

---- 

#### Natural polymers

Living things contain a lot of polymers too:

  * **Proteins**
  * **DNA**
  * **carbohydrates**

are all naturally occurying types of polymers.

